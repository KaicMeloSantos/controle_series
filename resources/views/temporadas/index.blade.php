@extends('layout')

@section('cabecalho')
	Temporadas
@endsection

@section('conteudo')
	<ul class="list-group">
		@foreach($temporadas as $temporada)
			
				<li class="list-group-item d-flex justify-content-between align-items-center">
					
						<a href="{{url('/temporadas/')}}/{{$temporada->id}}/episodios">
						Temporada {{$temporada->numero}}</a>

					<span class="badge badge-secondary">
						 {{$temporada->getEpisodioAssistidos()->count()}} / {{$temporada->episodios->count()}}</span>
				</li>
			
		@endforeach
	</ul>
@endsection 