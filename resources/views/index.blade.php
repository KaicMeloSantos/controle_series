@extends('layout')

@section('cabecalho')
			Series
@endsection

@section('conteudo')
@if(!empty($mensagem))
<div class="alert alert-success">
	{{$mensagem}}
</div>
@endif
	<a href="{{route('form_criar_serie')}}" class="btn btn-dark mb-2">Adicionar</a>

	<ul class="list-group">
		@foreach ($series as $key)
			<li class="list-group-item d-flex justify-content-between align-items-center"> 
				<span id="nome-serie-{{$key->id}}"> {{$key->nome}}</span>
				
				   <div class="input-group w-50" hidden id="input-nome-serie-{{ $key->id}}">
			            <input type="text" class="form-control" value="{{ $key->nome }}">
			            <div class="input-group-append">
			                <button class="btn btn-primary" onclick="editarSerie({{ $key->id }})">
			                    <i class="fas fa-check"></i>
			                </button>
			                @csrf
			            </div>
			        </div>

				<span class="d-flex">
					<button class="btn btn-info btn-sm mr-1" onclick="alteraNome({{$key->id}})"> 
						<i class="fas fa-edit"></i>
					</button>
					<a href=" {{url('/series/')}}/{{$key->id}}/temporadas" class="btn btn-info btn-sm mr-1">
					<i class="fas fa-external-link-alt"></i>
					</a>
					<form action="/series/{{$key->id}}" method="POST" onsubmit="return confirm('Tem certeza?')">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>	
					</form>
				</span>
			</li>
		@endforeach
	</ul>
	<script>
		function alteraNome(serieId){
			if(document.getElementById("nome-serie-"+serieId).hasAttribute("hidden")){
				document.getElementById("nome-serie-"+serieId).hidden = false;
				document.getElementById('input-nome-serie-'+serieId).hidden = true;
			}else{
				document.getElementById('input-nome-serie-'+serieId).removeAttribute("hidden");
				document.getElementById("nome-serie-"+serieId).hidden = true;
			}

		}

		function editarSerie(serieId){
			let formData = new FormData();

			const nome = document.querySelector("#input-nome-serie-"+serieId+" > input").value;
			const token = document.querySelector("input[name='_token']").value;

			formData.append("nome",nome);
			formData.append("_token",token);

			const url = "/series/"+serieId+"/editarSerie";

			fetch(url,{
				body:formData,
				method: 'POST'
			}).then(() => {
				alteraNome(serieId);
				document.getElementById("nome-serie-"+serieId).textContent = nome;
				// document.getElementById("nome-serie-"+serieId).hidden = true;
			});
		}
	</script>
@endsection