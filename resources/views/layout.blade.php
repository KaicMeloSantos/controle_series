<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
	<title></title>
	<script src="https://kit.fontawesome.com/b306cfcf4e.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light mb-2 d-flex justify-content-between">
  		<a class="navbar-brand" href="{{url('/series')}}">Navbar</a>

  		@auth
  		<a class="navbar-brand" href="#">sair</a>
  		@endauth

  		@guest
  		<a class="navbar-brand" href="#">Entrar</a>
  		@endguest
	</nav>
	<div class="container">
		<div class="jumbotron">
			<h1>@yield('cabecalho')</h1>
		</div>

		@yield('conteudo')
	</div>
	
</html>