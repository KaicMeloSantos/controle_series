@extends('layout')

@section('cabecalho')
			Adicionar Série
@endsection

@section('conteudo')
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif
		
	<form method="POST" action="{{url('/series/criar')}}">
		@csrf
		<div class="row">
			<div class="col col-8">
				<label for="nome">Nome</label>
				<div class="form-group">
					<input type="text" name="nome" id="nome" class="form-control">
				</div>
				<div class="form-group">
					<button class="btn btn-primary"> Adicionar</button>
				</div>
			</div>

			<div class="col col-2">
				<label for="nome">Nº temporadas</label>
				<div class="form-group">
					<input type="number" name="qtd_temporada" id="qtd_temporada" class="form-control">
				</div>
			</div>

			<div class="col col-2">
				<label for="nome">Ep. temporada</label>
				<div class="form-group">
					<input type="number" name="ep_por_temporada" id="ep_por_temporada" class="form-control">
				</div>
			</div>

		</div>	
	</form>
@endsection
