<?php

use Illuminate\Http\Request;
use App\Serie;
use App\{Temporada,Episodio};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("temporada",function(){ 
	// $temp = 
	// echo json_encode(Temporada::find(2));
	header('Content-Type: application/json');
	echo json_encode(Temporada::all());
});

Route::get('series',"SerieController@apiIndex");
Route::post('series',"SerieController@apiStore");

Route::get('series/{id}',"SerieController@apiGet");