<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/series',"SerieController@index")->name("listar_series");
Route::get('/series/criar',"SerieController@create")->name("form_criar_serie")->middleware("Autenticador");
Route::post('/series/criar',"SerieController@store")->middleware("Autenticador");
Route::post('/series/{id}/editarSerie',"SerieController@editaNome")->middleware("Autenticador"); 
Route::delete('/series/{id}',"SerieController@destroy")->middleware("Autenticador"); 

Route::get('/series/{id}/temporadas','TemporadasController@index');

Route::get('/temporadas/{temporada}/episodios','EpisodiosController@index'); 
Route::post('/temporadas/{temporada}/episodios/assistir','EpisodiosController@assistir')->middleware("Autenticador");
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get("/entrar","EntrarController@index");
Route::post("/entrar","EntrarController@entrar"); 

// Route::get("/registrar","RegistroController@create"); 
// Route::post("/registrar","RegistroController@store"); 