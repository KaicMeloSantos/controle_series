<?php

namespace App\Services;
use App\{Serie,Temporada,Episodio};
use Illuminate\Support\Facades\DB;

class RemovedorSerie{

	public function removerSerie(int $serieId){

        $nome = '';
        DB::transaction(function(){   
            $serie = Serie::find($serieId);
            $nome =  $serie->nome;
            $serie->temporadas->each(function(Temporada $temporada){
                $temporada->episodios->each(function(Episodio $episodio){
                    $episodio->delete();
                });
                $temporada->delete();
            });
            $serie->delete();
       });
            
             return $nome;
        }
       
}