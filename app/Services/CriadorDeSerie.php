<?php

namespace App\Services;
use App\serie;
use Illuminate\Support\Facades\DB;

class CriadorDeSerie{

	public function criarSerie($nome,$temporada,$ep_por_temporada){
    	// $nome =  $request->nome;
        DB::beginTransaction();

    	$serie = new serie();
    	$serie->nome = $nome;
    	$serie->save();

		// $temporada =  $request->qtd_temporada;
// $request->ep_por_temporada

        for ($i=1; $i <= $temporada ; $i++) { 
            $temporadas = $serie->temporadas()->create(['numero'=>$i]);

            for ($j=1; $j <= $ep_por_temporada; $j++) { 
               $temporadas->episodios()->create(['numero'=>$j]);
            }
        }
        DB::commit();
        return $serie;

	}
}