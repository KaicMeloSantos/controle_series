<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Serie;

class TemporadasController extends Controller
{
    public function index(int $id){
    	$temporadas = Serie::find($id)->temporadas;

    	return view('temporadas.index',compact('temporadas'));
    }	
}
