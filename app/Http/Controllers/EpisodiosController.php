<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Temporada,Episodio};

class EpisodiosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Temporada $temporada, Request $request)
    {
        // $episodios  = $temporada->episodios;
        return view('episodios.index',[
            "episodios" =>$temporada->episodios,
            "temporadaId" => $temporada->id,
            "mensagem"=>$request->session() ->get("mensagem")
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function assistir(Temporada $temporada, Request $request)
    {
        $episodioAssistido = $request->episodios;
        $temporada->episodios->each(function(Episodio $episodio) use ($episodioAssistido){
            $episodio->assistido = in_array($episodio->id, $episodioAssistido);
        });
        $temporada->push();
        $request->session()->flash('mensagem',"Episodios marcados com sucesso");

        return redirect()->back();
    }
}
