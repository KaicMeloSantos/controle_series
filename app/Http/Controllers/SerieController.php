<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Serie;
use App\Services\{CriadorDeSerie,RemovedorSerie};
use Auth;

class SerieController extends Controller
{
    public function index(Request $request){
        // if(!Auth::check()){
        //     echo "Não autorizada";
        //     exit();
        // }
        
    	$series = Serie::query()
    	->orderBY('nome')
    	->get();
		$mensagem = $request->session()->get('mensagem');
    	return view('index',compact('series','mensagem'));
    }
    public function apiIndex(Request $request){
     
        return Serie::all();
    }

    public function create(){

    	return view('create');
    }

    public function apiStore(Request $request){
        return response()->json(Serie::create(['nome'=>$request->nome]),201);
    }

    public function apiGet($id){
        return  Serie::find($id);
    }

    public function store(Request $request, CriadorDeSerie $criadorDeSerie){
		$rules = [
		        'nome' => 'required|min:2',
		    ];

		$customMessages = [
        'nome.required' => 'O campo nome é Obrigatório',
        'nome.min' => 'O campo precisa ter pelo menos 2 caracteres'
    	];

		$this->validate($request, $rules, $customMessages);

    	$serie = $criadorDeSerie->criarSerie($request->nome,$request->qtd_temporada,$request->ep_por_temporada);
        
        $request->session()
        ->flash(
            'mensagem',"Série {$serie->id} foi criada com sucesso {$serie->nome}"
        );
        //Pegar quantidade de temporadas digitada pelo usuario
   
    	return redirect()->route('listar_series');
    }

    public function destroy(Request $request, RemovedorSerie $remover){ 

    	$serie = $remover->removerSerie($request->id);

    	$request->session() 
    	->flash(
    		'mensagem',"Série $serie foi removida com sucesso!"
    	);
		return redirect()->route('listar_series');
    }

    public function editaNome(Request $request){
      
      $novoNome = $request->nome;
      $serie = Serie::find($request->id);

      $serie->nome = $novoNome;
      $serie->save();
 
    }

}
